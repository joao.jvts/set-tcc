package br.com.cctrans.set.tcc.controller.form;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

public class StudentForm {

	@NotNull
	@NotEmpty
	@Size(min = 3, max = 50)
	private String name;
	@NotNull
	@NotEmpty
	@CPF
	private Integer cpf;
	@NotEmpty
	@Size(min = 1)
	private List<String> phones;
	@NotNull
	@NotEmpty
	private Integer cnh;
	private String cnhPhoto;
	@NotNull
	@NotEmpty
	private String adress;
	@NotNull
	@Positive
	private Integer adressNumber;
	@Pattern(regexp = "^\\d{5}-\\d{3}$")
	private Integer zipCode;
	@NotNull
	@NotEmpty
	@Size(min = 2, max = 2)
	private String uf;
	@NotNull
	@NotEmpty
	private String State;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}

	public List<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		this.phones = phones;
	}

	public Integer getCnh() {
		return cnh;
	}

	public void setCnh(Integer cnh) {
		this.cnh = cnh;
	}

	public String getCnhPhoto() {
		return cnhPhoto;
	}

	public void setCnhPhoto(String cnhPhoto) {
		this.cnhPhoto = cnhPhoto;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Integer getAdressNumber() {
		return adressNumber;
	}

	public void setAdressNumber(Integer adressNumber) {
		this.adressNumber = adressNumber;
	}

	public Integer getZipCode() {
		return zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

}
