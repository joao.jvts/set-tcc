package br.com.cctrans.set.tcc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cctrans.set.tcc.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long>{
	
	Student findByName(String name);
}
