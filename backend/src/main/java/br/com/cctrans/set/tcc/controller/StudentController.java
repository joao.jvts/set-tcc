package br.com.cctrans.set.tcc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cctrans.set.tcc.model.Student;

@RestController
@RequestMapping("/student")
public class StudentController {
	
	@GetMapping("/{id}")
	public Student student(@PathVariable("id") Long id) {
		Student student = new Student();
		
		
		return student;
		
	}
	
	@PostMapping("/")
	public Student student(@RequestBody Student student) {
		return student;
	}
	
}
