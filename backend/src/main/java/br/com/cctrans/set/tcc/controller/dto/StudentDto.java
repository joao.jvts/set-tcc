package br.com.cctrans.set.tcc.controller.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.com.cctrans.set.tcc.model.Student;

public class StudentDto {
	private Long id;
	private String name;
	private Integer cpf;
	private Integer cnh;

//constructor
	public StudentDto(Student student) {
		super();
		this.id = student.getId();
		this.name = student.getName();
		this.cpf = student.getCpf();
		this.cnh = student.getCnh();
	}
	
//get
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public Integer getCpf() {
		return cpf;
	}
	public Integer getCnh() {
		return cnh;
	}
	
	
	public static List<StudentDto> converter(List<Student> students){
		return students.stream().map(StudentDto::new).collect(Collectors.toList());
	}

}
