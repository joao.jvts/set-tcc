package br.com.cctrans.set.tcc.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column (name="name")
	private String name;
	
	@Column (name="cpf")
	private Integer cpf;
	
	@Column (name="cnh")
	private Integer cnh;
	
	@Column (name="adress")
	private String adress;
	
	@Column (name="adressNumber")
	private Integer adressNumber;
	
	@Column (name="cep")
	private Integer zipCode;
	
	@Column (name="state")
	private String state;
	
	@Column (name="city")
	private String city;
	
}
