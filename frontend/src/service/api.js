import axios from "axios";

const api = axios.create({
  baseURL: 'http://localhost:3000'
})

export default {
  listStudents(callback) {
    api.get('/student').then(response => {
      callback(response.data)
    })
  },
  getStudentById(id, callback) {
    api.get(`/student/${id}`).then(response => {
      callback(response.data)
    })
  },
  async createStudent(student) {
    await api.post('/student', student)
  },
  async updateStudent(student) {
    await api.put(`/student/${student.id}`, student)
  },
  async deleteStudent(id) {
    await api.delete(`/student/${id}`)
  },

  listInstructors(callback) {
    api.get('/instructor').then(response => {
      callback(response.data)
    })
  },
  getInstructorById(id, callback) {
    api.get(`/instructor/${id}`).then(response => {
      callback(response.data)
    })
  },
  async createInstructor(instructor) {
    await api.post('/instructor', instructor)
  },
  async updateInstructor(instructor) {
    await api.put(`/instructor/${instructor.id}`, instructor)
  },
  async deleteInstructor(id) {
    await api.delete(`/instructor/${id}`)
  },
}