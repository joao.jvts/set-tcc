import axios from "axios";

const cepSevice = axios.create({
  baseURL: 'http://viacep.com.br/ws/'
})

export default {
  /**
   * 
   * @param {String|Number} cep cep to search on ViaCepAPI
   * @param {Function} callback callback for response
   */
  async getAddress(cep) {
    return (await cepSevice.get(`${cep}/json`)).data
  }
}