import moment from "moment";

const clearStringNumbers = str => str.replace(/\D/g, '')

const calcAge = bornDate => moment().diff(bornDate, 'years');

export default {
  clearStringNumbers,
  calcAge,
}