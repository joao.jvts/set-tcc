import api from '../../service/api'

export default {
  state: {
    loading: false,
    studentSelected: {},
    list: [],
  },

  actions: {
    getStudents({ commit }) {
      api.listStudents(students => {
        commit('setStudents', students)
      })
    },
    getStudentById({ commit }, id) {
      commit('setLoading', true)
      api.getStudentById(id, student => {
        commit('setSelectedStudent', student)
        commit('setLoading', false)
      })
    },
    async createStudent({ commit }, student) {
      await api.createStudent(student)
      commit('createStudent', student)
    },
    async updateStudent({ dispatch }, student) {
      await api.updateStudent(student)
      dispatch('getStudents')
    },
    async deleteStudent({ commit, dispatch }, id) {
      await api.deleteStudent(id)
      commit('deleteStudent', id)
      dispatch('getStudents')
    },
  },

  mutations: {
    setStudents(state, students) {
      state.list = students
    },
    setSelectedStudent(state, student) {
      state.studentSelected = student
    },
    setLoading(state, status) {
      state.loading = status
    },
    createStudent(state, newStudent) {
      state.list.push(newStudent)
    },
    deleteStudent(state, id) {
      const index = state.list.findIndex(student => student.id === id)
      state.list.slice(index, 1)
    }
  },
}