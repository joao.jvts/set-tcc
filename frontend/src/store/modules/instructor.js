import api from '../../service/api'

export default {
  state: {
    loading: false,
    instructorSelected: {},
    list: [],
  },
  actions: {
    getInstructors({ commit }) {
      api.listInstructors(instructors => {
        commit('setInstructors', instructors)
      })
    },
    getInstructorById({ commit }, id) {
      commit('setLoading', true)
      api.getInstructorById(id, instructor => {
        commit('setSelectedInstructor', instructor)
        commit('setLoading', false)
      })
    },
    async createInstructor({ commit }, instructor) {
      await api.createInstructor(instructor)
      commit('createInstructor', instructor)
    },
    async updateInstructor({ dispatch }, instructor) {
      await api.updateInstructor(instructor)
      dispatch('getInstructors')
    },
    async deleteInstructor({ commit, dispatch }, id) {
      await api.deleteInstructor(id)
      commit('deleteInstructor', id)
      dispatch('getInstructors')
    },
  },
  mutations: {
    setInstructors(state, instructors) {
      state.list = instructors
    },
    setSelectedInstructor(state, instructor) {
      state.instructorSelected = instructor
    },
    setLoading(state, status) {
      state.loading = status
    },
    createInstructor(state, newInstructor) {
      state.list.push(newInstructor)
    },
    deleteInstructor(state, id) {
      const index = state.list.findIndex(instructor => instructor.id === id)
      state.list.slice(index, 1)
    }
  },
}