export default {
  namespaced: true,

  state: {
    isAuthenticated: false,
    login: {}
  },

  actions: {
    login({ commit }, login) {
      commit('setUserInfo', login)
    },
    logout({ commit }) {
      commit('unsetUserInfo')
    },
  },

  mutations: {
    setUserInfo(state, login) {
      state.login = login
      state.isAuthenticated = true;
    },
    unsetUserInfo(state) {
      state.isAuthenticated = false;
      state.login = {}
    }
  },

  getters: {
    getIsAuthenticated: state => () => state.isAuthenticated,
  },
}