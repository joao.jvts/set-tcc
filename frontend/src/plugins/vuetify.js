import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import VueMask from 'v-mask'

Vue.use(VueMask);
Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#6a76ff",
      },
    },
  },
});
