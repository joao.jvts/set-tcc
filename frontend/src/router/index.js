import Vue from 'vue'
import VueRouter from 'vue-router'

import Main from '@/layouts/Main.vue'
import Login from '@/layouts/Login.vue'

import Home from '@/views/Home.vue'
import LoginView from '@/views/Login.vue'
import Student from '@/views/Student.vue'
import Instructor from '@/views/Instructor.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Home',
    name: 'Main Layout',
    component: Main,
    children: [
      {
        path: '/Home',
        name: 'Home',
        component: Home,
        meta: { requiresAuth: true },
      },
      {
        path: '/students',
        name: 'Alunos',
        component: Student,
        meta: { requiresAuth: true },
      },
      {
        path: '/instructors',
        name: 'Instrutores',
        component: Instructor,
        meta: { requiresAuth: true },
      },
    ]
  },
  {
    path: '/',
    component: Login,
    children: [
      {
        path: '/',
        component: LoginView,
        meta: { requiresAuth: false },
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const isAuth = store.getters['user/getIsAuthenticated']()
    if (!isAuth) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    }
  }
  next()
})

export default router
