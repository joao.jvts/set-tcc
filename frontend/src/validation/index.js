import { isCNH, isCPF } from 'validation-br'
import { validateCep } from 'validations-br'

const rules = {
  required: (value) => !!value || "Obrigatório.",
  email: (value) =>
    (value && /^[a-z0-9._]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i.test(value)) ||
    "Email inválido",
  minLength: (min) => value => (value && value.length >= min) || `Min ${min} caracteres`,
  maxLength: (max) => value => (value && value.length <= max) || `Max ${max} caracteres`,
  isNumber: value => !isNaN(value) || 'Valor inválido',
  positive: value => (value && value > 0) || 'Valor deve ser maior que zero.',
  cpf: (value) => (value && isCPF(value)) || 'CPF inválido',
  cnh: (value) => {
    if (!value) return 'CNH inválido'
    const cnh = value.toString()
    return (cnh && isCNH(cnh)) || 'CNH inválido'
  },
  cep: (value) => (value && validateCep(value)) || 'CEP inválido',
}

export default rules